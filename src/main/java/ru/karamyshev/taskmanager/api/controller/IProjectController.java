package ru.karamyshev.taskmanager.api.controller;

public interface IProjectController {
    
    void showProject();

    void clearProject();

    void createProject();
}
