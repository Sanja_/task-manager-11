package ru.karamyshev.taskmanager.api.controller;

public interface ITaskController { 
    
    void showTasks();

    void clearTasks();

    void createTasks();
}
