package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();
}
