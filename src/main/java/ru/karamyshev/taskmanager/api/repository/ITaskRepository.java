package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();
}
