package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
