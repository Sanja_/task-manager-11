package ru.karamyshev.taskmanager.bootstrap;

import ru.karamyshev.taskmanager.api.controller.ICommandController;
import ru.karamyshev.taskmanager.api.controller.IProjectController;
import ru.karamyshev.taskmanager.api.controller.ITaskController;
import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.service.ICommandService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.constant.ArgumentConst;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.constant.TerminalConst;
import ru.karamyshev.taskmanager.controller.CommandController;
import ru.karamyshev.taskmanager.controller.ProjectController;
import ru.karamyshev.taskmanager.controller.TaskController;
import ru.karamyshev.taskmanager.repository.CommandRepository;
import ru.karamyshev.taskmanager.repository.ProjectRepository;
import ru.karamyshev.taskmanager.repository.TaskRepository;
import ru.karamyshev.taskmanager.service.CommandService;
import ru.karamyshev.taskmanager.service.ProjectService;
import ru.karamyshev.taskmanager.service.TaskService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private IProjectService projectService = new ProjectService(projectRepository);

    private IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    private void inputCommand() {
        while (true) parsCommand(TerminalUtil.nextLine());
    }

    private void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) chooseResponsCommand(arg);
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        for (String arg : args) chooseResponsArg(arg.trim());
    }

    private void validateArgs(final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    private void chooseResponsArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            case ArgumentConst.ARGUMENTS: commandController.showArguments(); break;
            case ArgumentConst.COMMANDS: commandController.showCommands(); break;
            default: System.out.println(MsgCommandConst.ARGS_N_FOUND);
        }
    }

    private void chooseResponsCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT: commandController.showAbout(); break;
            case TerminalConst.VERSION: commandController.showVersion(); break;
            case TerminalConst.HELP: commandController.showHelp(); break;
            case TerminalConst.INFO: commandController.showInfo(); break;
            case TerminalConst.EXIT: commandController.exit(); break;
            case TerminalConst.ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.COMMANDS: commandController.showCommands(); break;
            case TerminalConst.TASK_LIST: taskController.showTasks(); break;
            case TerminalConst.TASK_CREATE: taskController.createTasks(); break;
            case TerminalConst.TASK_CLEAR: taskController.clearTasks(); break;
            case TerminalConst.PROJECT_LIST: projectController.showProject(); break;
            case TerminalConst.PROJECT_CREATE: projectController.createProject(); break;
            case TerminalConst.PROJECT_CLEAR: projectController.clearProject(); break;
            default: System.out.println(MsgCommandConst.COMMAND_N_FOUND);
        }
    }
}
