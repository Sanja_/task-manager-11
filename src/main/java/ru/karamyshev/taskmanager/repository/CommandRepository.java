package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.constant.ArgumentConst;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.constant.TerminalConst;
import ru.karamyshev.taskmanager.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, MsgCommandConst.DESCRIPTION_HELP
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, MsgCommandConst.DESCRIPTION_ABOUT
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, MsgCommandConst.DESCRIPTION_VERSION
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, MsgCommandConst.DESCRIPTION_EXIT
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, MsgCommandConst.DESCRIPTION_INFO
    );

    public static Command ARGUMENT = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, MsgCommandConst.DESCRIPTION_ARGUMENT
    );

    public static Command COMMAND = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, MsgCommandConst.DESCRIPTION_COMMAND
    );

    private static final Command TASK_CREATE = new Command(
        TerminalConst.TASK_CREATE, null, MsgCommandConst.TASK_CREATE
    );

    private static final Command TASK_CLEAR = new Command(
        TerminalConst.TASK_CLEAR, null, MsgCommandConst.TASK_REMOVE
    );

    private static final Command TASK_LIST = new Command(
        TerminalConst.TASK_LIST, null, MsgCommandConst.TASK_LIST
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, MsgCommandConst.PROJECT_CREATE
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, MsgCommandConst.PROJECT_REMOVE
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, MsgCommandConst.PROJECT_LIST
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(final Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(final Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }
}
