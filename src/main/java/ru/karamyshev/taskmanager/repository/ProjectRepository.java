package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project){
        projects.add(project);
    }

    @Override
    public void remove(final Project project){
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(){
        return projects;
    }

    @Override
    public void clear(){
        projects.clear();
    }
}
