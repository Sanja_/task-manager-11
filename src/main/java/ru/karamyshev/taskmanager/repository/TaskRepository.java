package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task){
        tasks.add(task);
    }

    @Override
    public void remove(final Task task){
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(){
        return tasks;
    }

    @Override
    public void clear(){
        tasks.clear();
    }
}
