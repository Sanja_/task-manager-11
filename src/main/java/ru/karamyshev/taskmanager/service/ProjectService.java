package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository IProjectRepository;

    public ProjectService(IProjectRepository IProjectRepository) {
        this.IProjectRepository = IProjectRepository;
    }

    @Override
    public void create(final String name) {
        final Project project = new Project();
        project.setName(name);
        IProjectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        IProjectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        IProjectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        IProjectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return IProjectRepository.findAll();
    }

    @Override
    public void clear() {
        IProjectRepository.clear();
    }
}
